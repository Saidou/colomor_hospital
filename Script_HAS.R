
rm(list = ls())

library(readxl)
library(dplyr, warn.conflicts = FALSE)
library(tidyr)
library(lubridate)
library(stringr)
library(ggplot2)
library(tibble)
library(scales)
library(forecast)
library(timetk, warn.conflicts = FALSE)
library(zoo)
library(readr)

sys_date <- Sys.setlocale("LC_TIME", "English")

add_total <- function(dta, totalcols = 2:3) {
  dta2 <- bind_rows(dta, apply(dta[, totalcols, drop = F], 2, sum, na.rm = TRUE))
  dta2[, -totalcols] <- lapply(dta2[, -totalcols], as.character)
  dta2[nrow(dta2), -totalcols] <- ""
  dta2[nrow(dta2), 1] <- "Total"
  dta2
}

# Common theme -----------------------------------------------------------------
theme = theme(legend.key.size = unit(0.5, "cm"),
              legend.key.width = unit(0.5,"cm"),
              legend.text = element_text(size = 7),
              plot.title = element_text(hjust = 0.0, size = 9),
              axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1))

# ------------------------------------------------------------------------------
## visualisation de 2020

covid_19_Gabon <- 
  read_excel("data/covid-19_Gabon.xlsx", sheet = "Global") %>%
  select(date, confirmed)%>%
  filter(date < "2021-01-20") %>%
  mutate(confirmed = rollmean(confirmed, k = 7 , fill = NA),
         date = as.Date(date))%>%
  filter(!is.na(confirmed))

dates_vline <- as.Date(c("2020-04-01", "2020-09-01", "2021-01-01"))
dates_vline <- which(covid_19_Gabon$date %in% dates_vline)

fig_0 <- 
  covid_19_Gabon %>%
  ggplot(aes(x = date, y = confirmed)) + 
  geom_line(size = 0.7, color = "blue4") +
  geom_vline(xintercept = as.numeric(covid_19_Gabon$date[dates_vline]), col = "red", lwd = 0.6, linetype = "dashed") +
  theme_classic() +
  labs(y = "",
       x = " ") +
  scale_y_continuous(breaks = seq(0, 200, 25)) +
  scale_x_date(date_breaks = "1 month", date_labels = "%b %Y", expand = c(0.05, 0.05)) +
  annotate(geom="text",x = as.Date("2020-06-15"), y = 145, label= "Phase 1") +
  annotate(geom="text",x = as.Date("2020-11-01"), y = 145, label= "Phase 2") +
  theme
##########----------------------------------------------------------------------


database <- read_excel("data/database_hospital.xlsx", sheet = "HAS")

data <- database %>%
  arrange(year) %>%
  mutate(total = rowSums(select(., medecine : urgency), na.rm = T),
         date = str_to_title(date)) %>%
  extract(date, "month", "^(...).*$")

# ------------------------------------------------------------------------------
data_1 <- data %>%
  select(month, year, total) %>%
  pivot_wider(names_from = year, values_from = total) %>%
  mutate_all(~(ifelse(. == 0, NA_real_, .))) %>%
  mutate(mean = round(rowMeans(select(., 2 : 9), na.rm = T)),
         excess = round(`2020` - mean),
         p_score = paste0(round((`2020` - mean) / mean * 100), "%"))


poisson_model1 <- glm(`2020` ~ `2019` + `2018` + `2017` + `2014` + `2013` + `2012`, family = quasipoisson(link = "log"), data = data_1)



coef_regr <-
  poisson_model1 %>% 
  summary() %>% 
  coefficients %>% 
  data.frame() %>%
  `colnames<-`(c("estimate", "str_error", "z_value", "p_value")) %>%
  select(-c(str_error, z_value))

# Prediction using our model ---------------------------------------------------

newdata1 <-
  data_1 %>% 
  select(`2012`, `2013`, `2014`, `2017`, `2018`, `2019`) %>% 
  as.data.frame()


# Probability calculated by R thru the predict function
estim_2020 <-
  newdata1 %>% 
  mutate(rank2020 = round(predict(poisson_model1, newdata = newdata1, type = "response")))

table_1 <-
  data_1 %>% 
  bind_cols(estim_2020 %>% select(rank2020)) %>%
  mutate(rank_excess = round(rank2020 - mean),
         rank_p_score = paste0(round((rank2020 - mean) / mean * 100), "%")) %>% 
  select(1 : 10, 14, 11, 12, 15, 13, 16) %>%
  add_total(2:14)

#===============================================================================

poisson_model2 <- glm(`2014` ~ `2013` + `2012`, family = quasipoisson(link = "log"), data = data_1)


newdata2 <-
  data_1 %>% 
  select(`2012`, `2013`, `2014`) %>% 
  as.data.frame()

estim_2015 <-
  newdata2 %>% 
  mutate(rank2015 = round(predict(poisson_model2, newdata = newdata2, type = "response")))

data_with_2015_complet <-
  data_1 %>% 
  bind_cols(estim_2015 %>% select(rank2015)) %>%
  mutate(`2015` = if_else(is.na(`2015`), rank2015, `2015`))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
poisson_model3 <- glm(`2015` ~ `2014` + `2013` + `2012`, family = quasipoisson(link = "log"), data = data_with_2015_complet)

newdata3 <-
  data_with_2015_complet %>% 
  select(`2012`, `2013`, `2014`, `2015`) %>% 
  as.data.frame()

estim_2016 <-
  newdata3 %>% 
  mutate(rank2016 = round(predict(poisson_model3, newdata = newdata3, type = "response")))

data_2 <-
  data_with_2015_complet %>% 
  bind_cols(estim_2016 %>% select(rank2016)) %>% 
  select(month, `2012`, `2013`, `2014`, `2015`, `2016` = rank2016, `2017`, `2018`, `2019`, `2020`)


tab_1 <- 
  data_2 %>%
  mutate(mean = round(rowMeans(select(., 2 : 9), na.rm = T)),
         excess = round(`2020` - mean),
         p_score = paste0(round((`2020` - mean) / mean * 100), "%")) %>%
  add_total(2:12)

## Phase 1 march, August 2020

tab_phase1e <- 
  tab_1 %>%
  filter(month %in% c("Mar", "Apr", "May", "Jun", "Jul", "Aug")) %>%
  add_total(2:12)

tab_phase1 <- 
  tab_1 %>%
  filter(month %in% c("Apr", "May", "Jun", "Jul", "Aug")) %>%
  add_total(2:12)

tab_phase2 <-
  tab_1 %>%
  filter(month %in% c("Sep", "Oct", "Nov", "Dec")) %>%
  add_total(2:12)
## data 3 ----------------------------------------------------------------------

data_3 <- 
  data_2 %>% 
  mutate(mean = round(rowMeans(select(., 2 : 9), na.rm = T))) %>% 
  pivot_longer(-month, names_to = "year", values_to = "HAS") %>%
  mutate(date = as.Date(parse_date_time(month, "%b")))

# fig 1 ------------------------------------------------------------------------

fig_1 <-
  data_3 %>%
  filter(year %in% c('2020', 'mean')) %>%
  mutate(year = if_else(year == "mean", "mean 2012-2019", year)) %>%
  ggplot(aes(date, HAS, color = year, group = year)) +
  geom_point() +
  geom_line(size = 0.8)  +
  theme_classic() +
  scale_color_manual(values = c("red", "blue4")) +
  scale_x_date(labels = date_format("%b"), expand = c(0, 0), date_breaks = "1 month") +
  labs(x = " ",
       y = "Number",
       color = "") +
  theme +
  theme(legend.position = "top")

# fig 2 ------------------------------------------------------------------------
fig_2 <- 
  data_2 %>%
  mutate(mean = round(rowMeans(select(., 2 : 9), na.rm = T)),
         p_score = round((`2020` - mean) / mean, 2),
         date = as.Date(parse_date_time(month, "%b"))) %>%
  select(date, p_score) %>%
  ggplot(aes(date, p_score)) +
  geom_bar(stat="identity", fill = "blue4") +
  theme_classic() +
  scale_y_continuous(labels = scales::percent_format(accuracy = 1L)) +
  scale_x_date(labels = date_format("%b"), expand = c(0.05, 0.05)) +
  labs(x = " ",
       y = " ") +
  theme

# Modeling of a time series ----------------------------------------------------

data_ts <- 
  data_2 %>% 
  pivot_longer(-month, names_to = "year", values_to = "deaths") %>% 
  mutate(date = paste0("01 ", month, " ", year),
         date = as.Date(parse_date_time(date, "%d-%b-%Y"))) %>%
  select(date, deaths) %>%
  tk_ts(.,freq = 12, silent = TRUE)

# fig 3 ------------------------------------------------------------------------

fig_3 <- 
  ggAcf(data_ts) +
  theme_classic() +
  labs(x = "Lag (month)",
       y = "ACF",
       title = "")


data_ts2020 <- 
  data_2 %>% 
  pivot_longer(-month, names_to = "year", values_to = "deaths") %>% 
  mutate(date = paste0("01 ", month, " ", year),
         date = as.Date(parse_date_time(date, "%d-%b-%Y"))) %>%
  select(date, deaths) %>%
  filter(date < "2020-01-01") %>%
  tk_ts(.,freq = 12, silent = TRUE)

fig_3_2020 <- 
  ggAcf(data_ts2020) +
  theme_classic() +
  labs(x = "Lag (month)",
       y = "ACF",
       title = "")

# calculating the simple moving average of the time series ---------------------

data_ts2 <- 
  data_2 %>% 
  pivot_longer(-month, names_to = "year", values_to = "deaths") %>% 
  mutate(date = paste0("01 ", month, " ", year),
         date = as.Date(parse_date_time(date, "%d-%b-%Y")),
         `death over time` = deaths,
         `simple moving average` = rollmean(`death over time`, k = 4 , fill = NA)) %>%
  select(4:6)

# %>%
#   pivot_longer(-date, names_to = "variable", values_to = "value")

fig_4 <- 
  data_ts2 %>%
  ggplot(aes(x = date, y = `simple moving average`)) + 
  geom_line(color = "blue4") +
  geom_smooth(se = F, color = "blue4") +
  theme_classic() +
  scale_x_date(date_breaks = "1 year", date_labels = "%Y") +
  labs(x = "",
       y = "Deaths",
       title = "4-month simple moving average") +
  theme

## Stringency Index ------------------------------------------------------------

covid_stringency <-
  read_csv("data/covid-stringency-index.csv") %>%
  filter(Code == "GAB",
         Day < "2021-01-15")

dates_vline2 <- as.Date(c("2020-03-01", "2020-09-01", "2021-01-01"))
dates_vline2 <- which(covid_stringency$Day %in% dates_vline2)

fig_index <- 
  covid_stringency%>%
  ggplot(aes(x = Day, y = stringency_index)) + 
  geom_line(size = 0.7, color = "blue4") +
  geom_vline(xintercept = as.numeric(covid_stringency$Day[dates_vline2]), col = "red", lwd = 0.6, linetype = "dashed") +
  theme_classic() +
  labs(y = "",
       x = " ") +
  scale_x_date(date_breaks = "1 month", date_labels = "%b %Y", expand = c(0.05, 0.05)) +
  annotate(geom="text",x = as.Date("2020-06-01"), y = 100, label= "Phase 1 enlarged") +
  annotate(geom="text",x = as.Date("2020-11-01"), y = 100, label= "Phase 2") +
  theme
  

# Render -----------------------------------------------------------------------

reportfolder <- "reports/"
filereportRmd <- "report_HAS.Rmd"
output_DIR <- paste0(reportfolder, "/Report_colomor_HAS_", format(Sys.Date(), "%Y-%m-%d"))
rmarkdown::render(filereportRmd, output_file = output_DIR, quiet = TRUE)

